package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoDbDemoApplication implements CommandLineRunner {

	@Autowired
	private AuthorRepository authorRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(MongoDbDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Customers found with findAll():");
	    System.out.println("-------------------------------");
	    for (Author customer : authorRepository.findAll()) {
	      System.out.println(customer.toString());
	    }
	    System.out.println();
		
	}

	
	
}
